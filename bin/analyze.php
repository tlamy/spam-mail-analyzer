#!/usr/bin/env php
<?php
declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

use Netwake\Spam\MimeHeaderParser;
use Netwake\Spam\NetworkList;
use Netwake\Spam\PostfixAccessList;
use Netwake\Spam\ReceivedAnalyzer;

$myServers = ['mx.google.com', 'mail.netwake.de'];

$inputFile = $argv[1] ?? 'php://stdin';

$headerParser = new MimeHeaderParser();
$analyzer = new ReceivedAnalyzer($headerParser);
$mailContents = file_get_contents($inputFile);
$headers = $headerParser->normalizeHeaders($mailContents);

$remoteIp = $analyzer->calculateRemoteIp($headers, $myServers);

echo $remoteIp . PHP_EOL;

$whitelist = new NetworkList();
$whitelist->addMany(
    [
        '35.190.247.0/24',
        '64.233.160.0/19',
        '66.102.0.0/20',
        '66.249.80.0/20',
        '72.14.192.0/18',
        '74.125.0.0/16',
        '108.177.8.0/21',
        '173.194.0.0/16',
        '209.85.128.0/17',
        '216.58.192.0/19',
        '216.239.32.0/19',
        '2001:4860:4000::/36',
        '2404:6800:4000::/36',
        '2607:f8b0:4000::/36',
        '2800:3f0:4000::/36',
        '2a00:1450:4000::/36',
        '2c0f:fb50:4000::/36',
        '172.217.0.0/19',
        '172.217.32.0/20',
        '172.217.128.0/19',
        '172.217.160.0/20',
        '172.217.192.0/19',
        '172.253.56.0/21',
        '172.253.112.0/20',
        '108.177.96.0/19',
        '35.191.0.0/16',
        '130.211.0.0/22',
    ],
    'Google'
);
$whitelist->addMany(
    [
        '212.227.126.128/25',
        '212.227.15.0/25',
        '212.227.17.0/27',
        '217.72.192.248/29',
        '82.165.159.0/26',
        '217.72.207.0/27',
        '217.72.192.64/26',
        '82.165.229.130',
        '82.165.230.22',
    ],
    'web.de'
);
$whitelist->addMany(
    [
        '213.165.64.0/23',
        '74.208.5.64/26',
        '74.208.4.192/26',
        ' 	82.165.159.0/24',
        '82.165.159.0/26',
        '217.72.207.0/27',
        '82.165.229.31',
        '82.165.230.21',
    ],
    'gmx.net'
);
$whitelist->addMany(
    [
        '157.55.9.128/25',
        '157.56.232.0/21',
        '157.56.240.0/20',
        '207.46.198.0/25',
        '207.46.4.128/25',
        '157.56.24.0/25',
        '157.55.157.128/25',
        '157.55.61.0/24',
        '157.55.49.0/25',
        '65.55.174.0/25',
        '65.55.126.0/25',
        '65.55.113.64/26',
        '65.55.94.0/25',
        '65.55.78.128/25',
        '111.221.112.0/21',
        '207.46.58.128/25',
        '111.221.69.128/25',
        '111.221.66.0/25',
        '111.221.23.128/25',
        '70.37.151.128/25',
        '157.56.248.0/21',
        '213.199.177.0/26',
        '157.55.225.0/25',
        '157.55.11.0/25',
        '40.92.0.0/15',
        '40.107.0.0/16',
        '52.100.0.0/14',
        '104.47.0.0/17',
        '2a01:111:f400::/48',
        '2a01:111:f403::/48',
        '51.4.72.0/24',
        '51.5.72.0/24',
        '51.5.80.0/27',
        '51.4.80.0/27',
        '2a01:4180:4051:0800::/64',
        '2a01:4180:4050:0800::/64',
        '2a01:4180:4051:0400::/64',
        '2a01:4180:4050:0400::/64',
        '157.55.0.192/26',
        '157.55.1.128/26',
        '157.55.2.0/25',
        '65.54.190.0/24',
        '65.54.51.64/26',
        '65.54.61.64/26',
        '65.55.111.0/24',
        '65.55.116.0/25',
        '65.55.34.0/24',
        '65.55.90.0/24',
        '65.54.241.0/24',
        '207.46.117.0/24',
        '207.68.169.173/30',
        '207.68.176.0/26',
        '207.46.132.128/27',
        '207.68.176.96/27',
        '65.55.238.129/26',
        '65.55.238.129/26',
        '207.46.116.128/29',
        '65.55.178.128/27',
        '213.199.161.128/27',
        '65.55.33.64/28',
        '65.54.121.120/29',
        '65.55.81.48/28',
        '65.55.234.192/26',
        '207.46.200.0/27',
        '65.55.52.224/27',
        '94.245.112.10/31',
        '94.245.112.0/27',
        '111.221.26.0/27',
        '207.46.50.192/26',
        '207.46.50.224',
    ],
    'Outlook'
);
$accessList = new NetworkList();
$postfixWrapper = new PostfixAccessList($accessList);
$postfixWrapper->load('/etc/postfix/access');
$postfixWrapper->load('/etc/postfix/access_cidr');

if (($message = $accessList->isListed($remoteIp)) !== null) {
    print("$remoteIp already listed: $message\n");
} elseif (($message = $whitelist->isListed($remoteIp)) !== null) {
    print("$remoteIp is whitelisted: $message\n");
} else {
    $subject = $analyzer->findSubject($headers);
    $blockSubject = date('Ymd_Hi') . ' ' . substr($subject, 0, 40) . (strlen($subject) > 40 ? '...' : '');
    $postfixWrapper->addIpToList($remoteIp, $blockSubject, '/etc/postfix/access');
    printf("Added %s with %s\n", $remoteIp, $blockSubject);
}
