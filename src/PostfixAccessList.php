<?php

declare(strict_types=1);

namespace Netwake\Spam;

use RuntimeException;

class PostfixAccessList
{
    /** @var NetworkList */
    private $list;

    /**
     * PostfixAccessList constructor.
     * @param NetworkList $list
     */
    public function __construct(NetworkList $list)
    {
        $this->list = $list;
    }

    /**
     * @param string $filename
     * @return NetworkList
     */
    public function load(string $filename): NetworkList
    {
        if (!is_file($filename) || !is_readable($filename)) {
            throw new RuntimeException('Could not load from ' . $filename);
        }

        foreach (file($filename) as $line) {
            $line = trim(preg_replace('/#.*/', '', $line));
            $fields = preg_split('/\s+/', $line, 3);
            if (count($fields) !== 3 || $fields[1] !== 'REJECT') {
                continue;
            }
            $this->list->addMany([$fields[0]], $fields[2]);
        }
        return $this->list;
    }

    /**
     * @param string $remoteIp
     * @param string $cause
     * @param string $accessFilename
     * @codeCoverageIgnore
     */
    public function addIpToList(string $remoteIp, string $cause, string $accessFilename): void
    {
        file_put_contents($accessFilename, sprintf("%s\tREJECT %s" . PHP_EOL, $remoteIp, $cause), FILE_APPEND);
        shell_exec("postmap $accessFilename");
    }
}
