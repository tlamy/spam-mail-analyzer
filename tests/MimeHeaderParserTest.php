<?php /** @noinspection LongLine */

declare(strict_types=1);

namespace Tests\Spam;

use Netwake\Spam\MimeHeaderParser;
use PHPUnit\Framework\TestCase;

final class MimeHeaderParserTest extends TestCase
{
    /** @var MimeHeaderParser */
    private $uut;

    public function setUp(): void
    {
        parent::setUp();
        $this->uut = new MimeHeaderParser();
    }

    /**
     * @test
     */
    public function normalizeHeadersCount(): void
    {
        $actual = $this->uut->normalizeHeaders(file_get_contents(__DIR__ . '/fixture/mail1.txt'));

        $this->assertIsArray($actual);
        $this->assertCount(26, $actual);
    }

    /**
     * @test
     */
    public function normalizeHeadersNoEmptyHeaders(): void
    {
        $actual = $this->uut->normalizeHeaders(file_get_contents(__DIR__ . '/fixture/mail1.txt'));

        $this->assertIsArray($actual);
        $this->assertNotContains('', $actual);
    }

    /**
     * @test
     */
    public function filterReceived(): void
    {
        $input = [
            'Received: 1',
            'Subject: huhu',
            'Received: 2',
            'Received: 3',
        ];

        $expected = [
            'Received: 1',
            'Received: 2',
            'Received: 3',
        ];
        $actual = $this->uut->filterReceived($input);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     * @noinspection LongLine
     */
    public function filterReceivedRealData(): void
    {
        $input = $this->uut->normalizeHeaders(file_get_contents(__DIR__ . '/fixture/mail1.txt'));

        $expected = [
            0 => 'Received: by 2002:a6b:3b46:0:0:0:0:0 with SMTP id i67csp911268ioa; Wed, 1 Jul 2020 14:13:29 -0700 (PDT)',
            1 => 'Received: from mta-88-223.sparkpostmail.com (mta-88-223.sparkpostmail.com. [192.174.88.223]) by mx.google.com with ESMTPS id 19si4854704plo.344.2020.07.01.14.13.29 for <tlamy.me@gmail.com> (version=TLS1_2 cipher=ECDHE-ECDSA-AES128-GCM-SHA256 bits=128/128); Wed, 01 Jul 2020 14:13:29 -0700 (PDT)',
        ];
        $actual = $this->uut->filterReceived($input);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     * @dataProvider receiveLineDataProvider
     * @param string $input
     * @param string|null $expectedSenderIp
     * @param string|null $expectedReceiveHost
     */
    public function getReceivingHostname(string $input, ?string $expectedSenderIp, ?string $expectedReceiveHost): void
    {
        $actual = $this->uut->getReceivingHostname($input);

        $this->assertSame($expectedReceiveHost, $actual);
    }

    /**
     * @test
     * @dataProvider receiveLineDataProvider
     * @param string $input
     * @param string|null $expectedSenderIp
     * @param string|null $expectedReceiveHost
     * @noinspection PhpUnusedParameterInspection
     */
    public function getSendingIp(string $input, ?string $expectedSenderIp, ?string $expectedReceiveHost): void
    {
        $actual = $this->uut->getSendingIp($input);

        $this->assertSame($expectedSenderIp, $actual, $input);
    }

    /**
     * @return array
     */
    public function receiveLineDataProvider(): array
    {
        return [
            [
                'Received: by 2002:a6b:3b46:0:0:0:0:0 with SMTP id i67csp911268ioa; Wed, 1 Jul 2020 14:13:29 -0700 (PDT)',
                null,
                '2002:a6b:3b46:0:0:0:0:0',
            ],
            [
                'Received: from mta-88-223.sparkpostmail.com (mta-88-223.sparkpostmail.com. [192.174.88.223]) by mx.google.com with ESMTPS id 19si4854704plo.344.2020.07.01.14.13.29 for <tlamy.me@gmail.com> (version=TLS1_2 cipher=ECDHE-ECDSA-AES128-GCM-SHA256 bits=128/128); Wed, 01 Jul 2020 14:13:29 -0700 (PDT)',
                '192.174.88.223',
                'mx.google.com',
            ],
            [
                'Received: from smtpauths5.setblue1.com (smtpauths5.setblue1.com [45.144.66.84]) by mail.netwake.de (Postfix) with ESMTPS id 0BAF526C8E1 for <stefan.lamy@netwake.de>; Thu,  2 Jul 2020 19:23:17 +0200 (CEST)',
                '45.144.66.84',
                'mail.netwake.de',
            ],
            [
                'Received: (qmail 2624287 invoked with uid 993); 2 Jul 2020 17:08:18 -0000',
                null,
                null,
            ],
        ];
    }
}
