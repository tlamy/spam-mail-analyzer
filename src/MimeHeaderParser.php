<?php

declare(strict_types=1);

namespace Netwake\Spam;

class MimeHeaderParser
{
    /**
     * @param string $message
     * @return string[]
     */
    public function normalizeHeaders(string $message): array
    {
        $lines = [];
        $input = explode("\n", $message);
        $inLineCount = count($input);
        for ($lineNumber = 0; $lineNumber < $inLineCount; $lineNumber++) {
            $line = trim($input[$lineNumber]);
            if ($line === '') {
                break;
            }
            while (preg_match('/^\s+/', $input[$lineNumber + 1]) > 0) {
                $lineNumber++;
                $line .= ' ' . trim($input[$lineNumber]);
            }
            $lines[] = $line;
        }
        return $lines;
    }

    /**
     * @param string[] $headers
     * @return string[]
     */
    public function filterReceived(array $headers): array
    {
        return array_values(
            array_filter(
                $headers,
                function (string $header): bool {
                    return strpos($header, 'Received: ') === 0;
                }
            )
        );
    }

    /**
     * @param string $headerString
     * @return string|null
     */
    public function getReceivingHostname(string $headerString): ?string
    {
        if (preg_match('/^Received: .*by (\S+)/', $headerString, $match) > 0) {
            return $match[1];
        }
        return null;
    }

    public function getSendingIp(string $headerString): ?string
    {
        if (preg_match('/^Received: .*from .* \[([^]]+)/', $headerString, $match) > 0) {
            return $match[1];
        }
        return null;
    }
}
