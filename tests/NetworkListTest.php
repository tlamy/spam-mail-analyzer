<?php

declare(strict_types=1);

namespace Tests\Spam;

use Netwake\Spam\NetworkList;
use PHPUnit\Framework\TestCase;

final class NetworkListTest extends TestCase
{
    /** @var NetworkList */
    private $uut;

    public function setUp(): void
    {
        parent::setUp();
        $this->uut = new NetworkList();
    }

    /**
     * @test
     * @dataProvider accessListProvider
     * @param string $ipAddress
     * @param string|null $expectedResult
     */
    public function isListedAccessHostList(string $ipAddress, ?string $expectedResult): void
    {
        $this->uut->addMany(['1.2.3.4', '10.0.0.0/8', '2a01:111:f400:fe4a::62b'], 'testme');
        $this->assertSame($expectedResult, $this->uut->isListed($ipAddress));
    }

    public function accessListProvider(): array
    {
        return [
            ['1.2.3.4', 'testme 1.2.3.4'],
            ['10.10.10.1', 'testme 10.0.0.0/8'],
            ['178.32.224.206', null],
            ['2a01:111:f400:fe4a::62b', 'testme 2a01:111:f400:fe4a::62b'],
            ['2a01:111:f400:fe4a::62c', null],
            ['127.0.0.1', null],
            ['', null],
            ['0.0.0.0', null],
        ];
    }
}
