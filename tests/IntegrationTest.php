<?php

declare(strict_types=1);

namespace Tests\Spam;

use Netwake\Spam\MimeHeaderParser;
use Netwake\Spam\ReceivedAnalyzer;
use PHPUnit\Framework\TestCase;

final class IntegrationTest extends TestCase
{
    /** @var ReceivedAnalyzer */
    private $uut;

    public function setUp(): void
    {
        parent::setUp();
        $this->uut = new ReceivedAnalyzer(new MimeHeaderParser());
    }

    /**
     * @test
     * @dataProvider fileProvider
     * @param string $filename
     * @param string|null $expectedSenderIp
     */
    public function senderIp(string $filename, ?string $expectedSenderIp): void
    {
        $stopMailservers = ['mx.google.com', 'mail.netwake.de'];
        $headers = (new MimeHeaderParser())->normalizeHeaders(file_get_contents($filename));
        $actual = $this->uut->calculateRemoteIp($headers, $stopMailservers);

        $this->assertSame($expectedSenderIp, $actual);
    }

    public function fileProvider(): array
    {
        return [
            'mail1' => [__DIR__ . '/fixture/mail1.txt', '192.174.88.223'],
            'mail2' => [__DIR__ . '/fixture/mail2.txt', '45.144.66.84'],
            'mail3' => [__DIR__ . '/fixture/mail3.txt', '54.240.1.20'],
            'mail4' => [__DIR__ . '/fixture/mail4.txt', '93.93.47.210'],
            'mail5' => [__DIR__ . '/fixture/mail5.txt', '212.27.42.4'],
            'mail6' => [__DIR__ . '/fixture/mail6.txt', '2607:f8b0:4864:20::844'],
            'mail7' => [__DIR__ . '/fixture/mail7.txt', '217.160.106.11'],
            'mail8' => [__DIR__ . '/fixture/mail8.txt', '217.160.106.11'],
            'mail9' => [__DIR__ . '/fixture/mail9.txt', '139.162.27.197'],
        ];
    }
}
