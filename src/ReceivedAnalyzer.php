<?php

declare(strict_types=1);

namespace Netwake\Spam;

use IPLib\Factory;

class ReceivedAnalyzer
{
    /** @var MimeHeaderParser */
    private $headerParser;

    public function __construct(MimeHeaderParser $headerParser)
    {
        $this->headerParser = $headerParser;
    }

    /**
     * @param string[] $headers
     * @param string[] $stopMailservers
     * @return string|null
     */
    public function calculateRemoteIp(array $headers, array $stopMailservers): ?string
    {
        $receivedHeaders = $this->headerParser->filterReceived($headers);
        if (count($receivedHeaders) === 0) {
            return null;
        }
        foreach (array_reverse($receivedHeaders) as $headerString) {
            $receivingHost = $this->headerParser->getReceivingHostname($headerString);
            if ($receivingHost === null) {
                continue;
            }
            if (in_array($receivingHost, $stopMailservers, true)) {
                $sendingIp = $this->headerParser->getSendingIp($headerString);
                $address = Factory::addressFromString(str_replace('IPv6:', '', $sendingIp));
                if ($address === null) {
                    continue;
                }
                $sendingIp = $address->toString();
                if ($sendingIp !== '127.0.0.1') {
                    return $sendingIp;
                }
            }
        }
        return null;
    }

    /**
     * @param string[] $headers
     * @return string
     */
    public function findSubject(array $headers): string
    {
        foreach ($headers as $header) {
            if (strpos($header, 'Subject: ') === 0) {
                $subject = substr($header, 9);
                return iconv(
                    'UTF-8',
                    'ASCII//TRANSLIT',
                    iconv_mime_decode($subject, ICONV_MIME_DECODE_CONTINUE_ON_ERROR, 'UTF-8')
                );
            }
        }
        return '<<No subject>>';
    }
}
