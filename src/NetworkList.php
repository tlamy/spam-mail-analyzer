<?php

namespace Netwake\Spam;

use IPLib\Factory;
use IPLib\Range\RangeInterface;

class NetworkList
{
    /** @var RangeInterface[] */
    private $list = [];

    /**
     * @param string[] $networkList
     * @param string|null $namePrefix
     */
    public function addMany(array $networkList, ?string $namePrefix): void
    {
        foreach ($networkList as $network) {
            $range = Factory::rangeFromString($network);
            if ($range === null) {
                continue;
            }
            $key = $namePrefix ? $namePrefix . ' ' . $network : $network;
            $this->list[$key] = $range;
        }
    }

    /**
     * @param string $ipAddress
     * @return string|null
     */
    public function isListed(string $ipAddress): ?string
    {
        $address = Factory::addressFromString($ipAddress);
        if ($address === null) {
            return null;
        }

        foreach ($this->list as $existing => $range) {
            if ($range->contains($address)) {
                return $existing;
            }
        }
        return null;
    }

    /**
     * @return RangeInterface[]
     */
    public function getList(): array
    {
        return $this->list;
    }
}
