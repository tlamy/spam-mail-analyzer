<?php

declare(strict_types=1);

namespace Tests\Spam;

use Netwake\Spam\MimeHeaderParser;
use Netwake\Spam\ReceivedAnalyzer;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class ReceivedAnalyzerTest extends TestCase
{
    /** @var ReceivedAnalyzer */
    private $uut;

    /** @var MimeHeaderParser|MockObject */
    private $headerParser;

    public function setUp(): void
    {
        parent::setUp();
        $this->headerParser = $this->createMock(MimeHeaderParser::class);
        $this->uut = new ReceivedAnalyzer($this->headerParser);
    }

    /**
     * @test
     */
    public function calculateRemoteIpNoStopServersMatchEmptyList(): void
    {
        $actual = $this->uut->calculateRemoteIp([], []);

        $this->assertNull($actual);
    }

    /**
     * @test
     */
    public function calculateRemoteIpNoStopServersMatch(): void
    {
        $actual = $this->uut->calculateRemoteIp([], ['mail.netwake.de']);

        $this->assertNull($actual);
    }

    /**
     * @test
     */
    public function findSubjectInEmptyHeadersList(): void
    {
        $actual = $this->uut->findSubject([]);
        $this->assertSame('<<No subject>>', $actual);
    }

    /**
     * @test
     */
    public function findSubject(): void
    {
        $actual = $this->uut->findSubject(
            [
                'SUBJECT: Muh',
                'subject:asd',
                'Subject: This is the real subject',
                'X-Spam: valid',
            ]
        );
        $this->assertSame('This is the real subject', $actual);
    }
}
