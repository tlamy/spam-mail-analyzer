#!/usr/bin/env php
<?php
declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

use Netwake\Spam\MimeHeaderParser;
use Netwake\Spam\ReceivedAnalyzer;

$myServers = ['mx.google.com', 'mail.netwake.de'];

$inputFile = $argv[1] ?? 'php://stdin';

$headerParser = new MimeHeaderParser();
$analyzer = new ReceivedAnalyzer($headerParser);
$mailContents = file_get_contents($inputFile);
$headers = $headerParser->normalizeHeaders($mailContents);

$remoteIp = $analyzer->calculateRemoteIp($headers, $myServers);

echo $remoteIp . PHP_EOL;
