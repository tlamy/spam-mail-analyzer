<?php

declare(strict_types=1);

namespace Tests\Spam;

use Netwake\Spam\NetworkList;
use Netwake\Spam\PostfixAccessList;
use PHPUnit\Framework\TestCase;
use RuntimeException;

final class PostfixAccessListTest extends TestCase
{
    /** @var PostfixAccessList */
    private $uut;

    public function setUp(): void
    {
        parent::setUp();
        $this->uut = new PostfixAccessList(new NetworkList());
    }

    /**
     * @test
     */
    public function loadAccessList(): void
    {
        $actual = $this->uut->load(__DIR__ . '/fixture/access');
        $this->assertCount(86, $actual->getList());
    }

    /**
     * @test
     */
    public function loadAccessCidrList(): void
    {
        $actual = $this->uut->load(__DIR__ . '/fixture/access_cidr');
        $this->assertCount(124, $actual->getList());
    }

    /**
     * @test
     */
    public function loadWithNonExistentFileThrows(): void
    {
        $this->expectException(RuntimeException::class);
        $this->uut->load('/path/does/not/exist');
    }
}
